package es.dpinfo.tema2.Exercise1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import es.dpinfo.tema2.Exercise1.dao.ContactDAO;
import es.dpinfo.tema2.Exercise1.models.Contact;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 14/11/16.
 */
public class ContactAdapter extends ArrayAdapter<Contact>{

    private Context myContext;

    public ContactAdapter(Context context) {
        super(context, R.layout.item_ex1_contact, ContactDAO.getInstance().getContactsList(context));
        this.myContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ContactHolder holder;

        if (item == null) {
            LayoutInflater inflater = LayoutInflater.from(myContext);
            item = inflater.inflate(R.layout.item_ex1_contact, null);
            holder = new ContactHolder();

            holder.itemContactName = (TextView) item.findViewById(R.id.item_contact_name);
            holder.itemContactNumPhone = (TextView) item.findViewById(R.id.item_contact_num_phone);
            holder.itemContactEmail = (TextView) item.findViewById(R.id.item_contact_email);

            item.setTag(holder);
        }
        else {
            holder = (ContactHolder) item.getTag();
        }

        holder.itemContactName.setText(" " + getItem(position).getmName());
        holder.itemContactNumPhone.setText(" " + getItem(position).getmNumPhone());
        holder.itemContactEmail.setText(" " + getItem(position).getmEmail());

        return item;
    }

    class ContactHolder {
        TextView itemContactName;
        TextView itemContactNumPhone;
        TextView itemContactEmail;
    }
}
