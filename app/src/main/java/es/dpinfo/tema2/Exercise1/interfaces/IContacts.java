package es.dpinfo.tema2.Exercise1.interfaces;

import android.content.Context;

/**
 * Created by dprimenko on 14/11/16.
 */
public interface IContacts {

    interface View {
        void setErrorMessage(int message);
    }

    interface Presenter {
        boolean showList();
        boolean addContact(Context context, String name, String phone, String email);
    }
}
