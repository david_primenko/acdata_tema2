package es.dpinfo.tema2.Exercise1.presenters;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.Exercise1.dao.ContactDAO;
import es.dpinfo.tema2.Exercise1.interfaces.IContacts;
import es.dpinfo.tema2.Exercise1.models.Contact;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 14/11/16.
 */
public class ContactsMvp implements IContacts.Presenter {

    private IContacts.View view;

    public ContactsMvp (IContacts.View view) {
        this.view = view;
    }

    @Override
    public boolean showList() {
        return !(ContactDAO.getInstance().isListEmpty());
    }

    @Override
    public boolean addContact(Context context, String name, String phone, String email) {
        boolean result = false;

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email)) {
            view.setErrorMessage(R.string.data_empty);
        }

        else if(phone.length() < 9) {
            view.setErrorMessage(R.string.invalid_phone);
        }

        else if (!email.matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")) {
            view.setErrorMessage(R.string.invalid_email);
        }

        else {
            ContactDAO.getInstance().addContact(context, new Contact(name, phone, email));
            result = true;
        }

        return result;
    }
}
