package es.dpinfo.tema2.Exercise1.models;

/**
 * Created by dprimenko on 14/11/16.
 */
public class Contact {


    //region Fields
    private String mName;
    private String mNumPhone;
    private String mEmail;
    //endregion

    //region Getter&Setter
    public String getmName() {
        return mName;
    }
    public void setmName(String mName) {
        this.mName = mName;
    }
    public String getmEmail() {
        return mEmail;
    }
    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }
    public String getmNumPhone() {
        return mNumPhone;
    }
    public void setmNumPhone(String mNumPhone) {
        this.mNumPhone = mNumPhone;
    }
    //endregion

    public Contact(String name, String numPhone, String email) {
        this.mName = name;
        this.mNumPhone = numPhone;
        this.mEmail = email;
    }

    @Override
    public String toString() {
        return mName + ";" +
                mNumPhone + ";" +
                mEmail;
    }

    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if(obj instanceof Contact) {
            if ((this.mNumPhone.equals(((Contact) obj).getmNumPhone())) || this.mEmail.equals(((Contact) obj).getmEmail())) {
                result = true;
            }
        }

        return result;
    }
}
