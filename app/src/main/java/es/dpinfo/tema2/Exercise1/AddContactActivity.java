package es.dpinfo.tema2.Exercise1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import es.dpinfo.tema2.Exercise1.interfaces.IContacts;
import es.dpinfo.tema2.Exercise1.presenters.ContactsMvp;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 14/11/16.
 */
public class AddContactActivity extends AppCompatActivity implements IContacts.View{

    private TextInputLayout tilNameContact;
    private TextInputLayout tilPhoneContact;
    private TextInputLayout tilEmailContact;
    private ContactsMvp presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex1_add_contact);
        presenter = new ContactsMvp(this);

        tilNameContact = (TextInputLayout) findViewById(R.id.til_name_contact);
        tilPhoneContact = (TextInputLayout) findViewById(R.id.til_phone_contact);
        tilEmailContact = (TextInputLayout) findViewById(R.id.til_email_contact);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_ex1_add_contact, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String name = tilNameContact.getEditText().getText().toString();
        String phone = tilPhoneContact.getEditText().getText().toString();
        String email = tilEmailContact.getEditText().getText().toString();

        boolean addResult = false;

        if (item.getItemId() == R.id.item_menu_add_contact) {
            addResult = presenter.addContact(getApplicationContext(), name, phone, email);

            if (addResult) {
                startActivity(new Intent(AddContactActivity.this, MainContactsActivity.class));
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setErrorMessage(int message) {
        Snackbar.make(findViewById(R.id.rl_add_contact), getString(message), Snackbar.LENGTH_LONG).show();
    }
}
