package es.dpinfo.tema2.Exercise1;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import es.dpinfo.tema2.Exercise1.adapters.ContactAdapter;
import es.dpinfo.tema2.Exercise1.interfaces.IContacts;
import es.dpinfo.tema2.Exercise1.presenters.ContactsMvp;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 14/11/16.
 */
public class MainContactsActivity extends AppCompatActivity implements IContacts.View{

    private IContacts.Presenter presenter;
    private TextView txvNoContacts;
    private ListView lwContacts;
    private FloatingActionButton fabAddContact;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex1_main_contacts);

        presenter = new ContactsMvp(this);
        txvNoContacts = (TextView) findViewById(R.id.txv_no_contacts);
        lwContacts = (ListView) findViewById(R.id.lw_contacts);
        fabAddContact = (FloatingActionButton) findViewById(R.id.fab_add_contact);

        fabAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainContactsActivity.this, AddContactActivity.class));
            }
        });

        showContent();
    }

    @Override
    public void setErrorMessage(int message) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_ex1_contacts_list, menu);

        return super.onCreateOptionsMenu(menu);
    }

    public void showContent() {

        ContactAdapter adapter = new ContactAdapter(this);
        lwContacts.setAdapter(adapter);

        if (presenter.showList()) {
            lwContacts.setVisibility(View.VISIBLE);
        }

        else {
            txvNoContacts.setVisibility(View.VISIBLE);
        }
    }
}
