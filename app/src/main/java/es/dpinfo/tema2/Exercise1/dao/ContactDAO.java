package es.dpinfo.tema2.Exercise1.dao;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dpinfo.tema2.Exercise1.models.Contact;
import es.dpinfo.tema2.IOUtilities;

/**
 * Created by dprimenko on 14/11/16.
 */
public class ContactDAO {
    private static ContactDAO ourInstance = new ContactDAO();
    private List<String[]> contactsCsv;
    private List<Contact> contactsList;
    private static final String CONTACTS_FILE = "contacts.txt";

    public static ContactDAO getInstance() {
        return ourInstance;
    }

    private ContactDAO() {
        contactsList = new ArrayList<>();
        contactsCsv = new ArrayList<>();
    }

    public void addContact(Context context, Contact contact) {
        try {
            IOUtilities.writeLineInternal(context, CONTACTS_FILE, contact.toString(), true);

        }
        catch(Exception e) {
            Log.d("Add contact exception", e.getMessage());
        }

    }

    public List<Contact> getContactsList(Context context) {

        contactsList.clear();

        if(readContactsFile(context)) {
            for (String[] array: contactsCsv) {
                addContactToList(new Contact(array[0], array[1], array[2]));
            }
        }
        
        return contactsList;
    }

    public boolean isListEmpty() {
        return contactsList.size() == 0;
    }

    private boolean readContactsFile(Context context) {
        boolean result = false;

        String filename = IOUtilities.getInternalFilesDir(context) + "/" + CONTACTS_FILE;

        contactsCsv = IOUtilities.readCSVFile(filename, 3, ";");

        if(contactsCsv.size() > 0) {
            result = true;
        }

        return  result;
    }

    private void addContactToList(Contact contact) {
        contactsList.add(contact);
    }
}
