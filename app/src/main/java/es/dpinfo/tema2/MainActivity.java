package es.dpinfo.tema2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import es.dpinfo.tema2.Exercise1.MainContactsActivity;
import es.dpinfo.tema2.Exercise2.MainAlarmsActivity;
import es.dpinfo.tema2.Exercise3.FertilePeriodActivity;
import es.dpinfo.tema2.Exercise4.Exercise4Activity;
import es.dpinfo.tema2.Exercise5.DownloadImageActivity;
import es.dpinfo.tema2.Exercise6.ConversorActivity;
import es.dpinfo.tema2.Exercise7.UploadActivity;

/**
 * Created by dprimenko on 3/12/16.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnEx1, btnEx2, btnEx3, btnEx4, btnEx5, btnEx6, btnEx7;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEx1 = (Button) findViewById(R.id.btn_exer_1);
        btnEx2 = (Button) findViewById(R.id.btn_exer_2);
        btnEx3 = (Button) findViewById(R.id.btn_exer_3);
        btnEx4 = (Button) findViewById(R.id.btn_exer_4);
        btnEx5 = (Button) findViewById(R.id.btn_exer_5);
        btnEx6 = (Button) findViewById(R.id.btn_exer_6);
        btnEx7 = (Button) findViewById(R.id.btn_exer_7);

        btnEx1.setOnClickListener(this);
        btnEx2.setOnClickListener(this);
        btnEx3.setOnClickListener(this);
        btnEx4.setOnClickListener(this);
        btnEx5.setOnClickListener(this);
        btnEx6.setOnClickListener(this);
        btnEx7.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()) {
            case R.id.btn_exer_1:
                intent = new Intent(MainActivity.this, MainContactsActivity.class);
                break;
            case R.id.btn_exer_2:
                intent = new Intent(MainActivity.this, MainAlarmsActivity.class);
                break;
            case R.id.btn_exer_3:
                intent = new Intent(MainActivity.this, FertilePeriodActivity.class);
                break;
            case R.id.btn_exer_4:
                intent = new Intent(MainActivity.this, Exercise4Activity.class);
                break;
            case R.id.btn_exer_5:
                intent = new Intent(MainActivity.this, DownloadImageActivity.class);
                break;
            case R.id.btn_exer_6:
                intent = new Intent(MainActivity.this, ConversorActivity.class);
                break;
            case R.id.btn_exer_7:
                intent = new Intent(MainActivity.this, UploadActivity.class);
                break;
        }

        startActivity(intent);
    }
}
