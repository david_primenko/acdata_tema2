package es.dpinfo.tema2.Exercise6;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import es.dpinfo.tema2.Exercise6.interfaces.IConversor;
import es.dpinfo.tema2.Exercise6.presenters.ConversorPresenter;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 20/11/16.
 */
public class ConversorActivity extends AppCompatActivity implements IConversor.View{

    private ConversorPresenter presenter;

    private EditText edtEur, edtDol;
    private RadioButton radioDolEur, radioEurDol;
    private Button btnConvert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex6_conversor);

        presenter = new ConversorPresenter(this);

        edtEur = (EditText) findViewById(R.id.edtEuros);
        edtDol = (EditText) findViewById(R.id.edtDolares);
        radioDolEur = (RadioButton) findViewById(R.id.radioDolEur);
        radioEurDol = (RadioButton) findViewById(R.id.radioEurDol);
        btnConvert = (Button) findViewById(R.id.btnConvert);

        radioDolEur.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    edtDol.setText("");
                    edtEur.setText("");
                    edtEur.setEnabled(false);
                    edtDol.setEnabled(true);
                }
            }
        });
        radioEurDol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    edtDol.setText("");
                    edtEur.setText("");
                    edtDol.setEnabled(false);
                    edtEur.setEnabled(true);
                }
            }
        });
        radioDolEur.setChecked(true);

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioDolEur.isChecked()) {

                    String value = presenter.exchangeToDolar(getApplicationContext(), edtDol.getText().toString());
                    edtEur.setText(value);
                }
                else {
                    edtDol.setText(presenter.exchangeToEuro(getApplicationContext(), edtEur.getText().toString()));
                }
            }
        });

        presenter.downloadFile(getApplicationContext());
    }

    @Override
    public void setMessageError(int message) {;
        Snackbar.make(findViewById(R.id.rl_conversor), getString(message), Snackbar.LENGTH_SHORT);
    }
}
