package es.dpinfo.tema2.Exercise6.presenters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import es.dpinfo.tema2.Exercise6.dao.ConversorDAO;
import es.dpinfo.tema2.Exercise6.interfaces.IConversor;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 20/11/16.
 */
public class ConversorPresenter implements IConversor.Presenter{

    IConversor.View view;

    public ConversorPresenter(IConversor.View view) {
        this.view = view;
    }

    @Override
    public String exchangeToDolar(Context context, String quantity) {
        String result = "";

        if (TextUtils.isEmpty(quantity)) {
            view.setMessageError(R.string.quantity_empty);
        }
        else if (ConversorDAO.getInstance().readFile(context, "conversor.txt")) {
            view.setMessageError(R.string.error_read_file);
        }
        else {
            result = ConversorDAO.getInstance().exchangeToDolar(quantity);
        }

        return result;
    }

    @Override
    public String exchangeToEuro(Context context, String quantity) {
        String result = "";

        if (TextUtils.isEmpty(quantity)) {
            view.setMessageError(R.string.quantity_empty);
        }
        else if (ConversorDAO.getInstance().readFile(context, "conversor.txt")) {
            view.setMessageError(R.string.error_read_file);
        }
        else {
            result = ConversorDAO.getInstance().exchangeToEuro(quantity);
        }

        return result;
    }

    @Override
    public void downloadFile(Context context) {
        ConversorDAO.getInstance().downloadFileAsyncHttpClient(context);
    }
}
