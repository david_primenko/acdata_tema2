package es.dpinfo.tema2.Exercise6.dao;

import android.content.Context;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.RestClient;

/**
 * Created by dprimenko on 20/11/16.
 */
public class ConversorDAO {
    private static ConversorDAO ourInstance = new ConversorDAO();

    public static ConversorDAO getInstance() {
        return ourInstance;
    }

    private double exchange;
    private String conversorFile;

    public double getExchange() {
        return exchange;
    }

    public void setExchange(double exchange) {
        this.exchange = exchange;
    }

    public ConversorDAO() {
    }

    public String exchangeToDolar(String quantity) {

        double value = 0;

        if(exchange != 0) {
            value = Double.parseDouble(quantity) / exchange;
        }

        return String.format("%.2f", value);
    }

    public String exchangeToEuro(String quantity) {
        double value = 0;

        if(exchange != 0) {
            value = Double.parseDouble(quantity) * exchange;
        }

        return String.format("%.2f", value);
    }

    public boolean readFile(Context context, String file) {
        boolean result = true;
        String path = context.getFilesDir().getPath() + "/" + file;
        double value = 0;
        try {
            value = Double.parseDouble(IOUtilities.readLine(path));
        }
        catch (Exception e) {
            result = false;
        }

        exchange = value;

        return result;
    }

    public void downloadFileAsyncHttpClient(Context context) {

        conversorFile = context.getFilesDir().getPath() + "/conversor.txt";

        RestClient.get("http://dpinfo.es/conversor.txt", new FileAsyncHttpResponseHandler(context) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                saveDownloadedFile(file, conversorFile);
            }
        });
    }

    private void saveDownloadedFile(File origin, String destination) {
        try {
            IOUtilities.copy(origin, destination);
        }
        catch (Exception e) {
            e.getMessage();
        }
    }
}
