package es.dpinfo.tema2.Exercise6.interfaces;

import android.content.Context;

/**
 * Created by dprimenko on 20/11/16.
 */
public interface IConversor {

    interface View {
        void setMessageError(int message);
    }
    interface Presenter {
        String exchangeToDolar(Context context, String quantity);
        String exchangeToEuro(Context context, String quantity);
        void downloadFile(Context context);
    }
}
