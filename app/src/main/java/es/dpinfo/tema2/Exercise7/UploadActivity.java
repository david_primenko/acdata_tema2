package es.dpinfo.tema2.Exercise7;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import es.dpinfo.tema2.R;
import es.dpinfo.tema2.RestClient;

/**
 * Created by dprimenko on 20/11/16.
 */
public class UploadActivity extends AppCompatActivity{

    private EditText etText;
    private TextView tvInfo;
    private Button btSubmit;

    public final static String WEB = "http://dpinfo.es/upload.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex7_upload);
        etText = (EditText) findViewById(R.id.etText7);
        btSubmit = (Button) findViewById(R.id.btSubmit7);
        tvInfo = (TextView) findViewById(R.id.tvInfo7);

        etText.setText("descarga.txt");

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subida();
            }
        });
    }



    private void subida() {

        String fichero = etText.getText().toString();
        File myFile;
        Boolean existe = true;
        myFile = new File(Environment.getExternalStorageDirectory().getPath(), fichero);
        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        final ProgressDialog progreso = new ProgressDialog(this);

        if (existe)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    // called before request is started
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    //progreso.setCancelable(false);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener(){
                        public void onCancel(DialogInterface dialog){
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }
                @Override
                public void onSuccess(int statusCode, Header[] headers, String response) {
                    // called when response HTTP status is "200 OK"
                    progreso.dismiss();
                    tvInfo.setText(response);
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    progreso.dismiss();
                    tvInfo.setText(response);
                }
            });
    }
}
