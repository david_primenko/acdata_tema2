package es.dpinfo.tema2.Exercise5.interfaces;

import android.content.Context;

/**
 * Created by dprimenko on 17/11/16.
 */
public interface IGallery {

    interface View {
        void setMessageError(int messageError);
    }

    interface Presenter {
        boolean downloadFile(Context context, String url);
        boolean readFile();
        String getNextLink();
        String getPreviousLink();
    }
}
