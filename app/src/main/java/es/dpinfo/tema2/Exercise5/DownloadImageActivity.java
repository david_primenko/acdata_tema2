package es.dpinfo.tema2.Exercise5;

import cz.msebera.android.httpclient.Header;
import es.dpinfo.tema2.Exercise5.dao.ImagesDAO;
import es.dpinfo.tema2.Exercise5.interfaces.IGallery;
import es.dpinfo.tema2.Exercise5.presenters.ImagesPresenter;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.R;
import es.dpinfo.tema2.RestClient;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.InputStream;

/**
 * Created by usuario on 15/11/16.
 */

public class DownloadImageActivity extends AppCompatActivity implements IGallery.View, View.OnClickListener{

    private ImagesPresenter presenter;
    private EditText edtUrl;
    private Button btnDownloadFile;
    private FloatingActionButton fabPreviousImg;
    private FloatingActionButton fabNextImg;
    private ImageView imvImage;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex5_download_image);

        presenter = new ImagesPresenter(this);

        edtUrl = (EditText) findViewById(R.id.edt_url);
        btnDownloadFile = (Button) findViewById(R.id.btn_download_file);
        imvImage = (ImageView) findViewById(R.id.imv_image);
        fabPreviousImg = (FloatingActionButton) findViewById(R.id.fab_previous_image);
        fabNextImg = (FloatingActionButton) findViewById(R.id.fab_next_image);

        fabPreviousImg.setOnClickListener(this);
        fabNextImg.setOnClickListener(this);

        btnDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(presenter.downloadFile(getApplicationContext(), edtUrl.getText().toString())) {
                    if(presenter.readFile()) {
                        fabPreviousImg.setVisibility(View.VISIBLE);
                        fabNextImg.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void setMessageError(int messageError) {
        Snackbar.make(findViewById(R.id.rl_exercise5), getString(messageError), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        url = "";

        switch (v.getId()) {
            case R.id.fab_next_image:
                url = presenter.getNextLink();
                break;
            case R.id.fab_previous_image:
                url = presenter.getPreviousLink();
                break;
        }

        try {

            Picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(R.drawable.
                            placeholder
                    )
                    .error(R.drawable.
                            error
                    )
                    .resize(
                            300, 400
                    )
                    .into(
                            imvImage
                    );
        }
        catch (Exception e) {
            Log.d("Url", url);
        }
    }
}
