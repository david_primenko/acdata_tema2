package es.dpinfo.tema2.Exercise5.presenters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import es.dpinfo.tema2.Exercise5.Errors;
import es.dpinfo.tema2.Exercise5.dao.ImagesDAO;
import es.dpinfo.tema2.Exercise5.interfaces.IGallery;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 17/11/16.
 */
public class ImagesPresenter implements IGallery.Presenter{

    private IGallery.View view;

    public ImagesPresenter(IGallery.View view) {
        this.view = view;
    }

    @Override
    public boolean downloadFile(Context context, String url) {

        boolean result = false;

        if (TextUtils.isEmpty(url)) {
            view.setMessageError(R.string.url_empty);
        }
        else {
            try {
                ImagesDAO.getInstance().downloadFileAsyncHttpClient(context, url);
                result = true;
                view.setMessageError(R.string.download_successs);
            }
            catch (Exception e){
                Log.d("Download error", e.getMessage());
            }
        }

        return result;
    }
    @Override
    public boolean readFile() {

        boolean result = false;

        if (ImagesDAO.getInstance().fillLinksListFromFile()) {
            view.setMessageError(R.string.read_file_success);
            result = true;
        }
        else {
            view.setMessageError(R.string.read_file_failed);;
        }

        return result;
    }

    @Override
    public String getNextLink() {
        return ImagesDAO.getInstance().getNextLink();
    }

    @Override
    public String getPreviousLink() {
        return ImagesDAO.getInstance().getPreviousLink();
    }


}
