package es.dpinfo.tema2.Exercise5.dao;


import android.content.Context;
import android.util.Log;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dpinfo.tema2.Exercise5.Errors;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.RestClient;

/**
 * Created by dprimenko on 17/11/16.
 */
public class ImagesDAO {

    private List<CharSequence> linksList;
    private String linksFile;
    private static ImagesDAO ourInstance = new ImagesDAO();
    private int iterator;

    public List<CharSequence> getLinksList() {
        return linksList;
    }

    public static ImagesDAO getInstance() {
        return ourInstance;
    }

    private ImagesDAO() {
        linksList = new ArrayList<>();
        iterator = 0;
    }

    public void downloadFileAsyncHttpClient(final Context context, String url) {

        linksFile = context.getFilesDir().getPath() + "/descarga.txt";

        RestClient.get(url, new FileAsyncHttpResponseHandler(context) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                saveDownloadedFile(file, linksFile);
            }
        });
    }

    public boolean fileExists(String file) {
        boolean result = false;

        if (IOUtilities.fileExists(file)) {
            result = true;
        }
        return result;
    }

    public String getPreviousLink() {

        String result = "";

        if (iterator > 0) {
            iterator--;
            int link = (iterator % linksList.size());
            result = linksList.get(link).toString();
        }
        else {
            iterator = (linksList.size() - 1);
            result = linksList.get(iterator).toString();
        }

        return result;
    }

    public String getNextLink() {
        iterator++;
        int link = (iterator % linksList.size());
        return linksList.get(link).toString();
    }

    private void saveDownloadedFile(File origin, String destination) {
        try {
            IOUtilities.copy(origin, destination);
        }
        catch (Exception e) {
            e.getMessage();
        }
    }

    public boolean fillLinksListFromFile() {

        boolean result = false;

        if (IOUtilities.fileExists(linksFile)) {
            linksList.clear();
            linksList = IOUtilities.readLinesTextFile(linksFile);
            result = true;
        }

        return result;
    }
}
