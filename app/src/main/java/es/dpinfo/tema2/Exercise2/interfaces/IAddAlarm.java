package es.dpinfo.tema2.Exercise2.interfaces;

/**
 * Created by dprimenko on 17/11/16.
 */
public interface IAddAlarm {

    interface View {
        void setErrorMessage(int message);
    }

    interface Presenter {
        boolean addAlarm(String texto, String tiempo);
    }
}
