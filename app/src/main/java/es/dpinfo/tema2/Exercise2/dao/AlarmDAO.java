package es.dpinfo.tema2.Exercise2.dao;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import es.dpinfo.tema2.Exercise2.models.Alarm;
import es.dpinfo.tema2.Exceptions.ExternalStorageException;
import es.dpinfo.tema2.IOUtilities;

/**
 * Created by dprimenko on 16/11/16.
 */
public class AlarmDAO {

    private String alarmsFile;
    private List<Alarm> alarmsList;
    private List<String[]> alarmsCsv;
    private static final String ALARMS_FILE = "alarms_file.txt";

    private static AlarmDAO ourInstance = new AlarmDAO();

    public static AlarmDAO getInstance() {
        return ourInstance;
    }

    private AlarmDAO() {
        alarmsFile = "";
        alarmsList = new ArrayList<>();
    }

    public void addAlarm(Alarm alarm) {
        try {
            IOUtilities.writeLineExternal(ALARMS_FILE, alarm.toString(), true);
        }
        catch(Exception e) {
            Log.d("Add alarm exception", e.getMessage());
        }

    }

    public void clearFile() {
        IOUtilities.writeExternal(ALARMS_FILE, "", false, "UTF-8");
    }

    public List<Alarm> getAlarmsList() {

        alarmsList.clear();

        if(readAlarmsFile()) {
            for (String[] array: alarmsCsv) {
                addAlarmToList(new Alarm(array[0], Long.parseLong(array[1]) * 60 * 1000));
            }
        }

        return alarmsList;
    }


    public boolean isListEmpty() {
        return alarmsList.size() == 0;
    }

    private boolean readAlarmsFile() {
        boolean result = false;
        String filename = "";

        try {
            filename = IOUtilities.getExternalStorageDir() + "/" + ALARMS_FILE;
            alarmsCsv = IOUtilities.readCSVFile(filename, 3, ";");

            if(alarmsCsv.size() > 0) {
                result = true;
            }
        }
        catch (ExternalStorageException e) {
            e.getMessage();
        }
        return  result;
    }

    private void addAlarmToList(Alarm contact) {
        alarmsList.add(contact);
    }
}
