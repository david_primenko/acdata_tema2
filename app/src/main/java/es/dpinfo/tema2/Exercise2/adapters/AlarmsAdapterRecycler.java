package es.dpinfo.tema2.Exercise2.adapters;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import es.dpinfo.tema2.Exercise2.dao.AlarmDAO;
import es.dpinfo.tema2.Exercise2.models.Alarm;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 17/11/16.
 */
public class AlarmsAdapterRecycler extends RecyclerView.Adapter<AlarmsAdapterRecycler.AlarmsViewHolder>{

    private List<Alarm> alarmList;
    private MyCountDownTimer timer;
    private ToneGenerator alert;


    public AlarmsAdapterRecycler() {
        this.alarmList = AlarmDAO.getInstance().getAlarmsList();
        alert = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
    }

    @Override
    public AlarmsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ex2_alarm, parent, false);
        return new AlarmsViewHolder(item);
    }

    @Override
    public void onBindViewHolder(AlarmsViewHolder holder, int position) {
        holder.txvAlarmText.setText(alarmList.get(position).getTexto());
        holder.txvAlarmTime.setText(showFormatedTime(alarmList.get(position).getTiempo()));
    }

    @Override
    public int getItemCount() {
        return alarmList.size();
    }

    public static class AlarmsViewHolder extends RecyclerView.ViewHolder {

        TextView txvAlarmText, txvAlarmTime;

        public AlarmsViewHolder(View view) {
            super(view);

            txvAlarmText = (TextView) itemView.findViewById(R.id.item_alarm_text);
            txvAlarmTime = (TextView) itemView.findViewById(R.id.item_alarm_time);
        }
    }

    public void clearAdapterContent() {

        if (timer != null) {
            timer.cancel();
        }
        alarmList.clear();
        notifyDataSetChanged();
    }

    public void startTimer() {
        if (alarmList.size() > 0) {
            long time = alarmList.get(0).getTiempo();
            timer = new MyCountDownTimer(time, 1000);
            timer.start();
        }
    }

    public void pauseTimer() {
        timer.cancel();
        timer = null;
    }

    public void resumeTimer() {
        //timer.resume();
    }

    private void deleteFirstAlarm() {
        if (alarmList.size() > 0) {
            alarmList.remove(0);

            notifyDataSetChanged();
        }
    }

    private String showFormatedTime(long millis) {

        long minutes = ((millis / 1000) / 60);
        long seconds = ((millis / 1000) % 60);

        String timeFormatted = String.format("%02d:%02d", minutes, seconds);

        return timeFormatted;
    }

    class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInTimer, long interval) {
            super(millisInTimer, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if(alarmList.size() > 0) {
                alarmList.get(0).setTiempo(millisUntilFinished);
                notifyDataSetChanged();
            }
        }

        @Override
        public void onFinish() {
            deleteFirstAlarm();
            notifyDataSetChanged();
            alert.startTone(ToneGenerator.TONE_CDMA_PIP, 1000);
            if (alarmList.size() > 0) {
                startTimer();
            }
        }
    }
}
