package es.dpinfo.tema2.Exercise2.presenters;

import android.text.TextUtils;

import es.dpinfo.tema2.Exercise2.dao.AlarmDAO;
import es.dpinfo.tema2.Exercise2.interfaces.IAddAlarm;
import es.dpinfo.tema2.Exercise2.models.Alarm;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 17/11/16.
 */
public class AddAlarmMvp implements IAddAlarm.Presenter{

    private IAddAlarm.View view;

    public AddAlarmMvp(IAddAlarm.View view) {
        this.view = view;
    }

    @Override
    public boolean addAlarm(String texto, String tiempo) {

        boolean result = false;

        if (TextUtils.isEmpty(texto) || TextUtils.isEmpty(tiempo)) {
            view.setErrorMessage(R.string.data_empty);
        }
        else {
            try {
                AlarmDAO.getInstance().addAlarm(new Alarm(texto, Integer.parseInt(tiempo)));
                result = true;
            }
            catch (Exception e) {
                view.setErrorMessage(R.string.error_add_alarm);
            }

        }

        return result;
    }
}
