package es.dpinfo.tema2.Exercise2.presenters;

import android.util.Log;

import es.dpinfo.tema2.Exercise2.dao.AlarmDAO;
import es.dpinfo.tema2.Exercise2.interfaces.IAlarms;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 16/11/16.
 */
public class AlarmsMvp implements IAlarms.Presenter{

    private boolean paused;
    private IAlarms.View view;

    public AlarmsMvp(IAlarms.View view) {
        this.view = view;
        this.paused = true;
    }

    @Override
    public boolean clearList() {

        boolean result = false;

        try {
            AlarmDAO.getInstance().clearFile();
            changeAlarmState();
            result = true;
        }
        catch (Exception e) {
            Log.d("Error de escritura", "no se pudo borrar el contenido del archivo");
        }

        return result;
    }

    @Override
    public boolean showAlarms() {
        return !(AlarmDAO.getInstance().isListEmpty());
    }

    @Override
    public void changeAlarmState() {
        if(paused) {
            view.changeControlAlarm(R.drawable.ic_pause, paused);
            paused = false;
        }
        else {
            view.changeControlAlarm(R.drawable.ic_play, paused);
            paused = true;
        }
    }
    
}
