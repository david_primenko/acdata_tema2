package es.dpinfo.tema2.Exercise2.interfaces;

/**
 * Created by dprimenko on 16/11/16.
 */
public interface IAlarms {

    interface View {
        void changeControlAlarm(int img, boolean state);
    }

    interface Presenter {
        boolean clearList();
        boolean showAlarms();
        void changeAlarmState();
    }
}
