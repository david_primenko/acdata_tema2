package es.dpinfo.tema2.Exercise2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import es.dpinfo.tema2.Exercise2.adapters.AlarmsAdapterRecycler;
import es.dpinfo.tema2.Exercise2.interfaces.IAlarms;
import es.dpinfo.tema2.Exercise2.presenters.AlarmsMvp;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 16/11/16.
 */
public class MainAlarmsActivity extends AppCompatActivity implements IAlarms.View{

    private AlarmsMvp presenter;
    private AlarmsAdapterRecycler adapter;
    private TextView txvNoAlarms;
    private RecyclerView rwAlarms;
    private FloatingActionButton fabAddAlarm;
    private FloatingActionButton fabControlAlarm;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ex2_main_alarms);

        presenter = new AlarmsMvp(this);
        txvNoAlarms = (TextView) findViewById(R.id.txv_no_alarms);
        rwAlarms = (RecyclerView) findViewById(R.id.rw_alarms);
        fabAddAlarm = (FloatingActionButton) findViewById(R.id.fab_add_alarm);
        fabControlAlarm = (FloatingActionButton) findViewById(R.id.fab_control_alarm);
        rwAlarms = (RecyclerView) findViewById(R.id.rw_alarms);

        setOnClickListeners();
        showContent();
    }

    private void setOnClickListeners() {
        fabAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainAlarmsActivity.this, AddAlarmActivity.class));
            }
        });

        fabControlAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.changeAlarmState();
            }
        });
    }

    private void showContent() {
        adapter = new AlarmsAdapterRecycler();
        rwAlarms.setLayoutManager(new LinearLayoutManager(this));
        rwAlarms.setAdapter(adapter);

        if (presenter.showAlarms()) {
            rwAlarms.setVisibility(View.VISIBLE);
        }
        else {
            txvNoAlarms.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_ex2_main_alarms, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_remove_alarms) {
            if(presenter.clearList()) {
                adapter.clearAdapterContent();
                txvNoAlarms.setVisibility(View.VISIBLE);
                startActivity(new Intent(MainAlarmsActivity.this, MainAlarmsActivity.class));
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void changeControlAlarm(int img, boolean state) {
        fabControlAlarm.setImageResource(img);
        if(state) {
            adapter.startTimer();
            fabAddAlarm.setVisibility(View.GONE);
        }
        else {
            adapter.pauseTimer();
            fabAddAlarm.setVisibility(View.VISIBLE);
        }
    }

}
