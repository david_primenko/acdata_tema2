package es.dpinfo.tema2.Exercise2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import es.dpinfo.tema2.Exercise2.interfaces.IAddAlarm;
import es.dpinfo.tema2.Exercise2.interfaces.IAlarms;
import es.dpinfo.tema2.Exercise2.presenters.AddAlarmMvp;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 16/11/16.
 */
public class AddAlarmActivity extends AppCompatActivity implements IAddAlarm.View, ActivityCompat.OnRequestPermissionsResultCallback{

    private TextInputLayout tilAlarmText;
    private TextInputLayout tilAlarmTime;
    private AddAlarmMvp presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ex2_add_alarm);

        presenter = new AddAlarmMvp(this);
        tilAlarmText = (TextInputLayout) findViewById(R.id.til_alarm_text);
        tilAlarmTime = (TextInputLayout) findViewById(R.id.til_alarm_time);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_ex2_add_alarm, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String text = tilAlarmText.getEditText().getText().toString();
        String time = tilAlarmTime.getEditText().getText().toString();

        boolean addResult = false;

        if (item.getItemId() == R.id.menu_item_add_alarm) {
            addResult = presenter.addAlarm(text, time);

            if (addResult) {
                startActivity(new Intent(AddAlarmActivity.this, MainAlarmsActivity.class));
                //finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setErrorMessage(int message) {
        Snackbar.make(findViewById(R.id.rl_add_alarm), getString(message), Snackbar.LENGTH_SHORT).show();
    }
}
