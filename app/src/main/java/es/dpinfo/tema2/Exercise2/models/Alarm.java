package es.dpinfo.tema2.Exercise2.models;

/**
 * Created by dprimenko on 16/11/16.
 */
public class Alarm {
    ;
    private String texto;
    private long tiempo;

    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Alarm(String texto, long tiempo) {
        this.texto = texto;
        this.tiempo = tiempo;
    }

    @Override
    public String toString() {
        return this.texto + ";" + this.tiempo;
    }
}
