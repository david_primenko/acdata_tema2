package es.dpinfo.tema2.Exercise3.presenters;

import android.content.Context;
import android.text.TextUtils;

import es.dpinfo.tema2.Exercise3.interfaces.IFertilePeriod;
import es.dpinfo.tema2.IOUtilities;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 19/11/16.
 */
public class FertilePeriodPresenter implements IFertilePeriod.Presenter{

    private IFertilePeriod.View view;

    public FertilePeriodPresenter(IFertilePeriod.View view) {
        this.view = view;
    }


    @Override
    public boolean saveFile(Context context, String file, String date, String cicle) {
        boolean result = false;

        if (TextUtils.isEmpty(cicle)) {
            view.setMessageError(R.string.cicle_empty);
        }
        else {
            IOUtilities.writeLineInternal(context, file, date + ";" + cicle, false);
        }

        return result;
    }

    @Override
    public int calculateFertilePeriod() {
        int resultado = 0;

        return resultado;
    }
}
