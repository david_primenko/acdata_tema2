package es.dpinfo.tema2.Exercise3;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;

import es.dpinfo.tema2.Exercise3.interfaces.IFertilePeriod;
import es.dpinfo.tema2.Exercise3.presenters.FertilePeriodPresenter;
import es.dpinfo.tema2.R;

/**
 * Created by dprimenko on 19/11/16.
 */
public class FertilePeriodActivity extends AppCompatActivity implements IFertilePeriod.View {


    private TextView txvDayLastMenst;
    private TextView txvDateLastMenst;
    private TextInputLayout tilCicle;
    private Button btnCalculateFertilePeriod;
    private CalendarView calendarFertilePeriod;
    private FertilePeriodPresenter presenter;
    private Date lastMenstDate;
    private RelativeLayout rlManageFertilePeriod;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex3_fertile_period);
        presenter = new FertilePeriodPresenter(this);

        rlManageFertilePeriod = (RelativeLayout) findViewById(R.id.rl_manage_fertile_period);
        txvDayLastMenst = (TextView) findViewById(R.id.txv_day_last_menst);
        txvDateLastMenst = (TextView) findViewById(R.id.txv_date_last_menst);
        tilCicle = (TextInputLayout) findViewById(R.id.til_cicle);
        btnCalculateFertilePeriod = (Button) findViewById(R.id.btn_calculate_fertile_period);
        calendarFertilePeriod = (CalendarView) findViewById(R.id.calendar_fertile_period);
        calendar = Calendar.getInstance();

        //calendarFertilePeriod.setMinDate(calendar.get(Calendar.YEAR));

        btnCalculateFertilePeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastMenstDate != null) {
                    //if (presenter.saveFile(getApplicationContext(), "fertil.txt", ))
                }

            }
        });

        rlManageFertilePeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void setMessageError(int message) {
        Snackbar.make(findViewById(R.id.rl_exercise3), getString(message), Snackbar.LENGTH_SHORT);
    }
}
