package es.dpinfo.tema2.Exercise3.interfaces;

import android.content.Context;

/**
 * Created by dprimenko on 19/11/16.
 */
public interface IFertilePeriod {

    interface View {
        void setMessageError(int message);
    }

    interface Presenter {
        boolean saveFile(Context context, String file, String date, String cicle);
        int calculateFertilePeriod();
    }
}
