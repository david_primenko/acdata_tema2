package es.dpinfo.tema2.Exceptions;

/**
 * Created by dprimenko on 16/11/16.
 */

public class ExternalStorageException extends Exception {
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    public ExternalStorageException() {
        super();
        this.message = "External Storage unmounted";
    }
    public ExternalStorageException(String message) {this.message = message;}
}
