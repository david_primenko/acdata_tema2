package es.dpinfo.tema2;

import android.content.Context;
import android.os.Environment;
import android.util.Log;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.List;;import es.dpinfo.tema2.Exceptions.ExternalStorageException;

/**
 * Created by dprimenko on 14/11/16.
 */

public class IOUtilities {

    public static List<String[]> readCSVFile (String file, int itemPerLine, String separator) {

        BufferedReader br = null;

        String csvLine = "";
        List<String[]> arrayList = new ArrayList<>();

        try {
            br = new BufferedReader(new FileReader(file));

            while ((csvLine = br.readLine()) != null) {
                arrayList.add(csvLine.split(separator));
            }
        }
        catch (IOException e) {
            Log.e("readcsv",e.getMessage());
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return arrayList;
    }

    public static List<CharSequence> readLinesTextFile(String file) {

        CharSequence charSequence;
        List<CharSequence> charSequences = new ArrayList<>();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));

            while((charSequence = br.readLine()) != null) {
                charSequences.add(charSequence);
            }
        }
        catch (FileNotFoundException e) {
            e.getMessage();
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            try {
                if(br != null) {
                    br.close();
                }
            }
            catch (IOException e) {
                e.getMessage();
            }
        }

        return charSequences;
    }

    public static String readLine(String file) {

        String string = "";
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));

            if ((string = br.readLine()) == null) {
                string = "";
            }

        }catch (FileNotFoundException e){
            Log.d("FileNotFoundException", e.getMessage());

        }catch (IOException e) {
            Log.d("IOException", e.getMessage());
        }
        finally {
            try {
                br.close();

            }catch (IOException e) {
                Log.d("IOException", e.getMessage());
            }
        }
        return string;
    }

    public static void writeLineExternal(String filename, String string, Boolean append) {
        File internalFile;
        try {
            internalFile = new File(getExternalStorageDir(), filename);
            writeLine(internalFile, string, append);
        }
        catch (ExternalStorageException e) {
            Log.e("writeLineExt",e.getMessage());
        }
    }

    public static void writeLineInternal(Context context, String filename, String string, Boolean append) {
        File internalFile;
        try {
            internalFile = new File(context.getFilesDir().getPath(), filename);
            writeLine(internalFile, string, append);
        }
        catch (Exception e) {
            e.getMessage();
        }
    }

    public static void writeInternal(Context context, String filename, String string, Boolean append, String charset) {

        try {
            File internalFile;
            internalFile = new File(context.getFilesDir().getPath(), filename);
            write(internalFile, string, append, charset);
        }
        catch (Exception e) {
            e.getMessage();
        }
    }

    public static void writeExternal(String filename, String string, Boolean append, String charset) {
        File externalFile;
        String externalDir = "";

        try {
            externalDir = getExternalStorageDir();
            externalFile = new File(externalDir, filename);
            write(externalFile, string, append, charset);
        }
        catch(ExternalStorageException e) {
            Log.e("writeExt",e.getMessage());
        }
    }

    public static boolean fileExists(String file) {
        boolean result = true;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException e) {
            result = false;
        }
        finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.getMessage();
            }
        }

        return result;
    }

    public static String getExternalStorageDir() throws ExternalStorageException{
        String result = "";

        if (!(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))) {
            throw new ExternalStorageException();
        }
        else {
            result = Environment.getExternalStorageDirectory().getPath();
            Log.e("external","mounted");
        }

        return result;
    }

    public static String getInternalFilesDir(Context context) {
        String result = "";
        result = context.getFilesDir().getPath();
        return result;
    }

    private static void writeLine(File filename, String string, Boolean append) {
        FileOutputStream fos = null;
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(filename, append);
            bw = new BufferedWriter(new OutputStreamWriter(fos));

            bw.write(string);
            bw.newLine();
        }
        catch (IOException e) {
            Log.e("writeLine",e.getMessage());
        }
        finally {
            if (bw != null) {
                try {
                    bw.close();
                }
                catch (IOException e) {
                    e.getMessage();
                }
            }
        }
    }

    private static void write(File filename, String string, Boolean append, String charset) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;

        try {
            fos = new FileOutputStream(filename, append);
            try {
                osw = new OutputStreamWriter(fos, charset);
            }
            catch (UnsupportedCharsetException e) {
                osw = new OutputStreamWriter(fos, Charset.forName("UTF-8"));
            }

            bw = new BufferedWriter(osw);
            bw.write(string);
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            }
            catch(IOException e) {
                    e.getMessage();
                }
        }
    }

    private static void writeArray(File filename, String[] array, Boolean append, String charset) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;

        try {
            fos = new FileOutputStream(filename, append);
            try {
                osw = new OutputStreamWriter(fos, charset);
            }
            catch (UnsupportedCharsetException e) {
                osw = new OutputStreamWriter(fos, Charset.forName("UTF-8"));
            }

            bw = new BufferedWriter(osw);

            for (String string:array) {
                bw.write(string);
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            }
            catch(IOException e) {
                e.getMessage();
            }
        }
    }

    private static void writeArray(File filename, String[] array, Boolean append, String charset, String separator) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;

        try {
            fos = new FileOutputStream(filename, append);
            try {
                osw = new OutputStreamWriter(fos, charset);
            }
            catch (UnsupportedCharsetException e) {
                osw = new OutputStreamWriter(fos, Charset.forName("UTF-8"));
            }

            bw = new BufferedWriter(osw);

            for (String string:array) {
                bw.write(string);
                bw.write(separator);
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            }
            catch(IOException e) {
                e.getMessage();
            }
        }
    }

    public static void copy(File origin, String destination) {
        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream(origin);
            out = new FileOutputStream(destination);

            int c;

            while ((c = in.read()) != -1) {
                out.write(c);
            }
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
            catch(IOException e) {
                e.getMessage();
            }
        }
    }

}
